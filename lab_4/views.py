from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

# Create your views here.

response={}
def page1(request):
    response={}
    return render(request, 'page1.html', response)
def page2(request):
    response={}
    return render(request, 'page2.html', response)
def page3(request):
    response={}
    return render(request, 'page3.html', response)
def page4(request):
    response={}
    return render(request, 'page4.html', response)
def make_schedule(request):
    schedules = Schedule.objects.all()
    response = {
        "schedules" : schedules
    }
    response['sched'] = ScheduleForm
    return render(request, 'makeschedule.html', response)

def schedule(request):
    form = ScheduleForm(request.POST or None)
    if(request.method == "POST"):
        if(form.is_valid()):
            response['day'] = request.POST.get("day")
            response['date'] = request.POST.get("date")
            response['time'] = request.POST.get("time")
            response['activity'] = request.POST.get("activity")
            response['place'] = request.POST.get("place")
            response['category'] = request.POST.get("category")
            submit_sched = Schedule(day=response['day'], date=response['date'], time=response['time'], activity=response['activity'], place=response['place'], category=response['category'])
            submit_sched.save()
            schedules = Schedule.objects.all()
            response['schedules'] = schedules
            return render(request, 'schedule.html', response)
        else :
            return render(request, "makeschedule.html", response)
    else :
        response['form'] = form
        return render(request, 'schedule.html', response)

def deleteschedule(request):
    schedules = Schedule.objects.all().delete()
    response['schedules'] = schedules
    return render(request, 'schedule.html', response)
