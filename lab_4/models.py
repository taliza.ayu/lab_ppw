from django.db import models
from django.utils import timezone 
from datetime import datetime, date

# Create your models here.

class Schedule(models.Model):
    day = models.CharField(max_length=15) 
    date = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=200) 
    place = models.CharField(max_length=200) 
    category = models.CharField(max_length=200) 

def publish(self):
    self.save()

def __str__(self):
    return self.activity