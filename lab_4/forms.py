from django import forms

class ScheduleForm(forms.Form):
    day = forms.CharField(label='Day')
    date = forms.DateField(label='Date', required=False, widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label='Time', required=False, widget=forms.TimeInput(attrs={'type' : 'time'}))
    activity = forms.CharField(label='Activity')
    place = forms.CharField(label='Place')
    category = forms.CharField(label='Category')