# from django.contrib import admin
# from django.urls import path
# from . import views

# urlpatterns = [
#     path('', views.index, name='index'),
#     path('data/', views.data, name='data'),
# ]

from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'lab_9'),
    path('data', views.data, name = 'data'),
]