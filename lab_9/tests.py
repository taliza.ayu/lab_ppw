from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import data
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
# Create your tests here.
class Lab9UnitTest(TestCase):
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code,200)

    def test_lab_9_fav_template(self):
        response = Client().get('/lab_9/')
        self.assertTemplateUsed(response, 'fav.html')
    
    def test_lab_9_using_index_func(self):
        found = resolve('/lab_9/')
        self.assertEqual(found.func, index)
    
    # def test_lab_9_using_data_func(self):
    #     found = resolve('/lab_9/data/')
    #     self.assertEqual(found.func, data)
    
class Lab9FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab9FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab9FunctionalTest, self).tearDown()

    def test_title(self):
        self.browser.get('http://taliza-lab6.herokuapp.com/lab_9/')
        time.sleep(5)
        self.assertIn("Books", self.browser.title)

    # def test_header(self):
    #     browser.get('http://taliza-lab6.herokuapp.com/lab_9/')
    #     content = browser.find_element_by_tag_name('p').value_of_css_property('color')
    #     self.assertIn('rgba(0, 0, 0, 1)', content)
    #     time.sleep(2)

    
    # def test_header_with_css_property(self):
    #     self.browser.get('http://taliza-lab6.herokuapp.com/lab_9/')
    #     header_text = self.browser.find_element_by_class_name('table-heading').value_of_css_property('text-align')
    #     time.sleep(5)
    #     self.assertIn('center', header_text)
