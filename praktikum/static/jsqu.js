$(document).ready(function() {
	$.ajax({
		url: "data",
		datatype: 'json',
		success: function(data){
			var obj = jQuery.parseJSON(data)
			renderHTML(obj);
		}
	})
});
	
var container = document.getElementById("demo");
function renderHTML(data){
	htmlstring = "<tbody>";
	for(i = 0; i < data.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
		"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
		"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
	} 
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
}

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}

// $(document).ready(function() {
// 	$("#myInput").on("keyup", function(e) {
// 		q = e.currentTarget.value.toLowerCase()
// 		console.log(q)

// 		$.ajax({
// 			url: "data/?q=" + q,
// 			datatype: 'json',
// 			success: function(data){
// 				$('tbody').html('')
// 				var result ='<tr>';
// 				for(var i = 0; i < data.items.length; i++) {
// 					result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
// 					"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
// 					"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
// 					"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
// 					"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
// 					"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
// 					"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
// 				}
// 				$('tbody').append(result);
// 			},
// 			error: function(error){
// 				alert("Books not found");
// 			}
// 		})
// 	});
// });

// var counter = 0;
// function favorite(clicked_id){
// 	var btn = document.getElementById(clicked_id);
// 	if(btn.classList.contains("checked")){
// 		btn.classList.remove("checked");
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
// 		counter--;
// 		document.getElementById("counter").innerHTML = counter;
// 	}
// 	else{
// 		btn.classList.add('checked');
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
// 		counter++;
// 		document.getElementById("counter").innerHTML = counter;
// 	}
// }
