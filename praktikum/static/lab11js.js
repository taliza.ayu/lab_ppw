$(document).ready(function() {
	getfav();
	$.ajax({
		url: "data/",
		datatype: 'json',
		success: function(data){
			var obj = jQuery.parseJSON(data)
			renderHTML(obj);
		}
	})
});
	
var container = document.getElementById("demo");
function renderHTML(data){
	htmlstring = "<tbody>";
	for(i = 0; i < data.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
		"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
		"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
		"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
	} 
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
	getfav();
}

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}


var counter = 0;
function getfav(){
	$.ajax({
		type : 'GET',
		url : '/lab_11/getfav/',
		dataType : 'json',
		success : function(data) {

			for(var i=1; i<=data.message.length; i++) {
				console.log(data.message[i-1]);
				var id = data.message[i-1];
				var td = document.getElementById(id);
				if(td!=null){
					td.className='clicked';
					td.src='https://image.flaticon.com/icons/svg/291/291205.svg';
				}  
				$('#counter').html(data.message.length);	  
			}
			// $('tbody').html(print);
		}
	});  
};
  
function favorite(id) {
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	var ini = document.getElementById(id);
	var yellowstar = 'https://image.flaticon.com/icons/svg/291/291205.svg';
	var blankstar = 'https://image.flaticon.com/icons/svg/149/149222.svg';
	if (ini.className=='checked') {
		$.ajax({
			url: "/lab_11/unfav/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
			id: id,
			},
			success: function(result) {
				counter=result.message;
				ini.className='';
				ini.src=blankstar;
				$('#counter').html(counter);
			},
			error : function (errmsg){
				alert("Something is wrong");
			}
		});
	} else {
		$.ajax({
			url: "/lab_11/fave/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
				id: id,
			},
			success: function(result) {
				console.log(ini);
				counter=result.message;
				ini.className='checked';
				ini.src=yellowstar;
				$('#counter').html(counter);
			   
		   },
		   error : function (errmsg){
			   alert("Something is wrong");
		   }
		});
	}
}