from django.db import models

# Create your models here.

class Subscriber(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=30, primary_key=True, unique=True)
    password = models.CharField(max_length=12)


