$(document).ready(function () {
    $('#submit_subscribe').prop('disabled', true);
    $('.unsubSuccess').hide();

    var flag = [false, false, false, false];
    //index 0 untuk nama, index 1 untuk email, index 2 untuk password, dan index 3 untuk email exist
    
    //ambil elemen nama
    $('#name').on('input', function () {
        var input = $(this);
        check(input, 0);
        checkButton();
    });

    //cek email saat ngetik
    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email");
            check(input, 1);
        }, 1000);
        checkButton();
    });

    //cek password
    $('#password').on('input', function () {
        var input = $(this);
        check(input, 2);
        checkButton();
    });

    var check = function (input, arr) {
        //jika arr == 1, cek email
        if (arr == 1) {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            //apakah sesuai sama reg atau tidak
            var is_el = reg.test(input.val());
            //jika email sesuai penulisan, di cek email udah ada apa belum
            if (is_el) {
                flag[arr] = true;
                checkEmail(input.val());
                return
            } else {
                $(input).parent().removeClass('alert-validate2');
                flag[arr] = false;
                checkButton();
            }
        } else {
            var is_el = input.val();
        }
        if (is_el) {
            hideValidate(input);
            flag[arr] = true;
        } else {
            flag[arr] = false;
            showValidate(input)
        }
    };

    function showValidate(input) {
        input.parent().addClass('alert-validate');
    }

    function hideValidate(input) {
        input.parent().removeClass('alert-validate');
    }

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "checkemail/",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    showValidate(inptEmail);
                    inptEmail.parent().addClass('alert-validate2');
                    flag[3] = false;
                    checkButton()
                } else {
                    hideValidate(inptEmail);
                    inptEmail.parent().removeClass('alert-validate2');
                    flag[3] = true;
                    checkButton()
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var bttn = $('#submit_subscribe');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                bttn.prop('disabled', true);
                return
            }
        }
        bttn.prop('disabled', false);
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: '',
                headers:{
                    "X-CSRFToken": csrftoken
                },
                data: $('form').serialize(),
                success: function (status) {
                    if (status.status_subscribe) {
                        var title1 = "<p style='font-size: 18px'><strong>Thank you!</strong></p>";
                        var link = "<a href=''>Click here to go back</a>";
                        $("form").remove();
                        $(".signup-form").append(link);
                    } else {
                        var title1 = "<p style='font-size: 22px'><strong>Oops something went wrong</strong></p>";
                        var link = "<a href=''>Please try again!</a>";
                        $("form").remove();
                        $(".signup-form").append(link);
                    }
                    $(".judul").replaceWith(title1);
                    window.location.reload();
                    for (var i = 0; i < flag.length; i++) {
                        flag[i] = false
                    }
                    $("#submit_subscribe").prop('disabled', true);
                    $('#name').val("");
                    $('#email').val("");
                    $('#password').val("");
                },
                error: function(error) {
                    alert("Error, cannot connect to server")
                }
            });
        });
    });

    $.ajax({
        url: "listsubscriber/",
        datatype: 'json',
        success: function(data){
            $('tbody').html('')
            var result ='<tr>';
            for(var i = 0; i < data.all_subs.length; i++) {
                result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td class='align-middle text-center'>" + data.all_subs[i].name +"</td>" +
                "<td class='align-middle text-center'>" + data.all_subs[i].email +"</td>" +
                "<td class='align-middle text-center'>" + "<a " + "data-email=" + data.all_subs[i].email + " class='text-white btn btn-danger unsubscribe-button' float-right role='button' aria-pressed='true'>" + "Unsubscribe" + "</a></td></tr>";
            }
            $('tbody').append(result);
        },
        error: function(error){
            alert("No subscriber");
        }
    })

    $('#demo').on('click', 'td .unsubscribe-button', function() {
        var email = $(this).attr('data-email');
        unsubscribe(email);
        
    });
});
function unsubscribe(email){
    $.ajax({
        method: "POST",
        url: "unsubscribe/",
        data: {email: email},
        success: function(){
            $('.unsubSuccess').show();
            window.location.reload();
        },
    })
}

// $(document).ready(function () {
//     $('#submit_subscribe').prop('disabled', true);

//     var flag = [false, false, false, false];
//     //index 0 untuk nama, index 1 untuk email, index 2 untuk password, dan index 3 untuk email exist
    
//     //ambil elemen nama
//     $('#name').on('input', function () {
//         var input = $(this);
//         check(input, 0);
//         checkButton();
//     });

//     //cek email saat ngetik
//     var timer = null;
//     $('#email').keydown(function () {
//         clearTimeout(timer);
//         timer = setTimeout(function () {
//             var input = $("#email");
//             check(input, 1);
//         }, 1000);
//         checkButton();
//     });

//     //cek password
//     $('#password').on('input', function () {
//         var input = $(this);
//         check(input, 2);
//         checkButton();
//     });

//     var check = function (input, arr) {
//     //cek email
//         if (arr == 1) {
//             var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//             //apakah sesuai sama reg atau tidak
//             var is_el = reg.test(input.val());
//             //jika email sesuai penulisan, di cek email udah ada apa belum
//             if (is_el) {
//                 flag[arr] = true;
//                 checkEmail(input.val());
//                 return
//             } else {
//                 $(input).parent().removeClass('alert-validate2');
//                 flag[arr] = false;
//                 checkButton();
//             }
//         } else {
//             var is_el = input.val();
//         }
//         if (is_el) {
//             hideValidate(input);
//             flag[arr] = true;
//         } else {
//             flag[arr] = false;
//             showValidate(input)
//         }
//     };

//     function showValidate(input) {
//         input.parent().addClass('alert-validate');
//     }

//     function hideValidate(input) {
//         input.parent().removeClass('alert-validate');
//     }

//     var checkEmail = function (email) {
//         var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//         $.ajax({
//             method: "POST",
//             url: "checkemail/",
//             headers:{
//                 "X-CSRFToken": csrftoken
//             },
//             data: {email: email},
//             success: function (response) {
//                 var inptEmail = $("#email");
//                 if (response.is_email) {
//                     showValidate(inptEmail);
//                     inptEmail.parent().addClass('alert-validate2');
//                     flag[3] = false;
//                     checkButton()
//                 } else {
//                     hideValidate(inptEmail);
//                     inptEmail.parent().removeClass('alert-validate2');
//                     flag[3] = true;
//                     checkButton()
//                 }
//             },
//             error: function (error) {
//                 alert("Error, cannot get data from server")
//             }
//         })
//     };

//     var checkButton = function () {
//         var bttn = $('#submit_subscribe');
//         for (var x = 0; x < flag.length; x++) {
//             if (flag[x] === false) {
//                 bttn.prop('disabled', true);
//                 return
//             }
//         }
//         bttn.prop('disabled', false);
//     };

//     $(function () {
//         var csrftoken = $("[name=csrfmiddlewaretoken]").val();
//         $('form').on('submit', function (e) {
//             e.preventDefault();
//             $.ajax({
//                 method: "POST",
//                 url: '',
//                 headers:{
//                     "X-CSRFToken": csrftoken
//                 },
//                 data: $('form').serialize(),
//                 success: function (status) {
//                     if (status.status_subscribe) {
//                         var title1 = "<p style='font-size: 18px'><strong>Thank you!</strong></p>";
//                         var link = "<a href=''>Click here to go back</a>";
//                         $("form").remove();
//                         $(".signup-form").append(link);
//                     } else {
//                         var title1 = "<p style='font-size: 22px'><strong>Something went wrong</strong></p>";
//                         var link = "<a href=''>Please, try again!</a>";
//                         $("form").remove();
//                         $(".signup-form").append(link);
//                     }
//                     $(".judul").replaceWith(title1);
//                     for (var i = 0; i < flag.length; i++) {
//                         flag[i] = false
//                     }
//                     $("#submit_subscribe").prop('disabled', true);
//                     $('#name').val("");
//                     $('#email').val("");
//                     $('#password').val("");
//                 },
//                 error: function(error) {
//                     alert("Error, cannot connect to server")
//                 }
//             });
//         });
//     });
// });



