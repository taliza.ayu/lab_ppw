from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.subscribe, name='subscribe'),
    path('checkemail/', views.checkEmail, name="checkEmail"),
    path('listsubscriber/', views.listSubscriber, name='listSubscriber'),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),

]

# urlpatterns = [
#     path('', views.signup, name = 'signup'),
#     path('subscribed/', views.signup, name = 'signed'),
#     path('emailValidation/', views.signup, name = 'emailValidation'),
# ]

