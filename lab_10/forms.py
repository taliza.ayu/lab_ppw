from django import forms

class FormSubscriber(forms.Form):
    name = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Name', 'id' : 'name'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, min_length=8, max_length=12, widget=forms.TextInput(attrs={'type': 'password', 'label': 'Password', 'id' : 'password'}))


# from django import forms

# class RegisForm(forms.Form):
#     attrs = {
#         'class': 'form-control',
#         'type' : 'text'
#     }

#     email_attrs = {
#         'class': 'form-control',
#         'type': 'email'
#     }

#     pass_attrs = {
#         'class': 'form-control',
#         'type': 'password'
#     }

#     name = forms.CharField(label="Name:")
#     email = forms.EmailField(label="Email:", required=True, widget=forms.EmailInput(attrs=email_attrs))
#     password = forms.CharField(label="Password:", required=True, widget=forms.TextInput(attrs=pass_attrs))

