from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.status, name='status'),
    path('profile/', views.profile, name='profile'),
]