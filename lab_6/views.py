from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm
from django.http import HttpResponse

# Create your views here.

response={}
def status(request):
    form = StatusForm(request.POST or None)
    status_message = Status.objects.all()
    response = {
        "status_message" : status_message
    }
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        message = request.POST.get("message")
        Status.objects.create(message = message)
        return redirect('status')
    else :
        return render(request, 'status.html', response)

def profile(request):
    response={}
    return render(request, 'profile.html', response)