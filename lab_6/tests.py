from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status
from .views import status, profile
from .forms import StatusForm
import unittest
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_to_do_list_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'status.html')

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(message="testing status")
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_lab6_using_create_form_func(self):
        found= resolve('/lab_6/')
        self.assertEqual(found.func, status)

    def test_lab_6_html_text(self):
        request = HttpRequest()
        response = status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello! how are you?', html_response)

    def test_lab_6_html_text(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Welcome to My Profile!', html_response)
        self.assertIn('TALIZA AYU', html_response)
        self.assertIn('Undergraduate student at Faculty of Computer Science, University of Indonesia', html_response)

    def test_lab_6_html_img(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn("/static/img/foto1.JPG", html_response)
    
class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_new_status(self):
        selenium = self.selenium
        selenium.get('https://taliza-lab6.herokuapp.com/lab_6/')
        time.sleep(2)
        status_box = selenium.find_element_by_class_name('form-control')
        status_box.send_keys('Coba Coba')
        status_box.submit()
        time.sleep(2)
        assert 'Coba Coba' in selenium.page_source
        time.sleep(3)

    def test_check_title_status(self):
        selenium = self.selenium
        selenium.get('https://taliza-lab6.herokuapp.com/lab_6/')
        assert 'Write Status' in selenium.title
        # self.assertIn("Write Status", selenium.title)

    def test_check_title_profile(self):
        selenium = self.selenium
        selenium.get('https://taliza-lab6.herokuapp.com/lab_6/profile/')
        assert 'My Profile' in selenium.title
        # self.assertIn("My Profile", selenium.title)

    def test_css_button(self):
        selenium = self.selenium
        selenium.get('https://taliza-lab6.herokuapp.com/lab_6/')
        button = selenium.find_element_by_tag_name("button")
        self.assertIn('submit', button.get_attribute("type"))

    def test_button_to_profile(self):
        selenium = self.selenium
        selenium.get('https://taliza-lab6.herokuapp.com/lab_6/')
        button = selenium.find_element_by_tag_name("a")
        self.assertIn('btn', button.get_attribute("class"))
    