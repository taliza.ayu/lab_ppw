from django import forms
from .models import Status

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    message = forms.CharField(label='Whats on your mind?', widget=forms.Textarea(attrs=attrs), required=True)
