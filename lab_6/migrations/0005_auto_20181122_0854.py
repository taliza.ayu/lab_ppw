# Generated by Django 2.1.1 on 2018-11-22 01:54

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_6', '0004_auto_20181122_0841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 22, 1, 54, 32, 906612, tzinfo=utc)),
        ),
    ]
