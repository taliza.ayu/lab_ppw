from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

# Create your views here.
app_name = 'lab_11'
response={}
def login(request):
	return render(request, 'login.html')

def data(request):
    json_list = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    json_parse = json.dumps(json_list)
    return HttpResponse(json_parse)

def logout(request):
    request.session.flush()
    logout(request)
    return render(request, 'login.html', {})
    

def index(request):
    if  request.user.is_authenticated and "fave" not in request.session:
        request.session["email"] = request.user.email
        request.session["sessionid"]=request.session.session_key
        request.session["fave"] = []
    return render(request, 'books.html')

@csrf_exempt
def fave(request):
    if(request.method=="POST"):
        lst = request.session["fave"]
        if request.POST["id"] not in lst :
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["fave"])
        request.session["fave"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")
    
@csrf_exempt
def unfav(request):
    if(request.method=="POST"):
        lst = request.session["fave"]
        if request.POST["id"] in lst :
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["fave"])
        request.session["fave"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")
    
def getfav(request):
    if request.user.is_authenticated :
        if(request.method=="GET"):
            if request.session["fave"] is not None:
                response["message"] = request.session["fave"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)
