import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .views import *

# Create your tests here.
class Lab11Test(TestCase):
    def test_lab_11_url_exist(self):
        response = Client().get('/lab_11/')
        self.assertEqual(response.status_code, 200)

    def test_json_url_exist(self):
        response = Client().get('/lab_11/data/')
        self.assertEqual(response.status_code, 200)

    # def test_html_templates(self):
    #     request = HttpRequest()
    #     response = login(request)
    #     html_response = response.content.decode("utf8")
    #     self.assertIn("Books", html_response)

    def test_lab_11_using_to_do_list_template(self):
        response = Client().get('/lab_11/')
        self.assertTemplateUsed(response, 'books.html')

    def test_lab_extra_using_indexlab11_func(self):
        found = resolve('/lab_11/')
        self.assertEqual(found.func, index)

    def test_lab_11_using_index_func(self):
        found = resolve('/lab_11/data/')
        self.assertEqual(found.func, data)

    def test_final_url_logout_exist(self):
        response = Client().get('/lab_11/logout/')
        self.assertEqual(response.status_code, 302)
