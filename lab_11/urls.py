from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views
from . import views as viewsfunc
# from .views import *


urlpatterns = [
    path('', viewsfunc.index, name = 'lab_11'),
    path('data/', viewsfunc.data, name = 'data'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    path('fave/', viewsfunc.fave, name="fave"),
    path('unfav/', viewsfunc.unfav, name="unfav"),
    path('getfav/', viewsfunc.getfav, name="getfav"),
]
